package BinaryTree.Traversals;

import BinaryTree.Traversals.BinaryTree.Node;

import java.util.LinkedList;
import java.util.Queue;

//Level Order traversal is also known as Breadth-First Traversal since it traverses all the nodes at each level before going to the next level (depth).
public class levelOrderTraversal {
    Node root;
    Queue<Node> queue = new LinkedList<>();


    private void levelOrderIterative(Node root) // Time Complexity – O(n)  Space Complexity – O(n) The Queue size is equal to the number of the nodes.
    {
        if (root == null) {
            return;
        }
        queue.add(root);

        while (!queue.isEmpty()) // If queue is not empty than loop until we covered all left and right elements
        {
            Node tempNode = queue.poll(); // Take the front element and remove it from the Queue
            System.out.print(tempNode.data + " ");
            if (tempNode.left != null) {
                queue.add(tempNode.left);
            }
            if (tempNode.right != null) {
                queue.add(tempNode.right);
            }
        }
    }

    //O(n^2) in worst case. For a skewed tree, printGivenLevel() takes O(n) time
    public static void levelOrderRecursive(Node root) {
        if (root == null) {
            return;
        }
        int height = height(root);
        for (int i = 1; i <= height; i++) {
            printAtEachLevel(root, i);
        }

    }

    private static void printAtEachLevel(Node root, int level) {
        if (root == null) {
            return;
        }
        if (level == 1) {
            System.out.print(root.data + " ");

        } else {
            printAtEachLevel(root.left, level - 1);
            printAtEachLevel(root.right, level - 1);
        }
    }


   static int  height(Node root) {
        if (root == null) {
            return 0;
        } else {
            int leftHeight = height(root.left);
            int rightHeight = height(root.right);

            if (leftHeight > rightHeight) {
                return leftHeight + 1;
            } else {
                return rightHeight + 1;
            }

        }
    }


    public static void main(String[] args) {
        levelOrderTraversal levelOrderTraversal = new levelOrderTraversal();

        // Inputting Data
        levelOrderTraversal.root = new Node(1);
        levelOrderTraversal.root.left = new Node(10);
        levelOrderTraversal.root.right = new Node(30);
        levelOrderTraversal.root.left.left = new Node(40);
        levelOrderTraversal.root.right.right = new Node(50);

        // Recursive way
        levelOrderTraversal.levelOrderRecursive(levelOrderTraversal.root);


        System.out.println();

        levelOrderTraversal.levelOrderIterative(levelOrderTraversal.root);
    }


}
