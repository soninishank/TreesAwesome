package BinaryTree.Traversals;

import BinaryTree.Traversals.BinaryTree.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class postOrderTraversal {
    Node root;
    Stack<Node> stack = new Stack<>();


    private void postOrderRecursive(Node root) {
        // Base condition you must apply in every Binary Tree Questions
        if (root == null) {
            return;
        }
        postOrderRecursive(root.left);

        postOrderRecursive(root.right);

        System.out.print(root.data + "  ");
    }


    private List<Integer> postOrderIterative(Node root) {
        List<Integer> list = new ArrayList<>();
        // Base condition you must apply in every Binary Tree Questions
        if (root == null) {
            return list;
        }
        stack.push(root);
        while (!stack.isEmpty()) {
            Node curr = stack.pop();
            list.add(0, curr.data);
            if (curr.left != null) {
                stack.push(curr.left);
            }
            if (curr.right != null) {
                stack.push(curr.right);
            }
        }
        return list;
    }


    public static void main(String[] args) {
        postOrderTraversal postOrderTraversal = new postOrderTraversal();

        // Inputting Data
        postOrderTraversal.root = new Node(1);
        postOrderTraversal.root.left = new Node(2);
        postOrderTraversal.root.right = new Node(3);
        postOrderTraversal.root.left.left = new Node(4);
        postOrderTraversal.root.left.right = new Node(5);

        // Recursive way
        postOrderTraversal.postOrderRecursive(postOrderTraversal.root);

        System.out.println();

        // Iterative Pre Order Traversal
        List<Integer> list = postOrderTraversal.postOrderIterative(postOrderTraversal.root);

        System.out.println(list);

    }


}
