package BinaryTree.Traversals;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.TreeMap;

import BinaryTree.Traversals.BinaryTree.Node;

public class verticalOrderTraversal {

    Node root;
    TreeMap<Integer, TreeMap<Integer, PriorityQueue<Integer>>> map = new TreeMap<>();
    List<List<Integer>> list = new ArrayList<>();

    public List<List<Integer>> verticalTraversal(Node root) {
        dfs(root, 0, 0, map);
        for (TreeMap<Integer, PriorityQueue<Integer>> ys : map.values()) {
            list.add(new ArrayList<>());
            for (PriorityQueue<Integer> nodes : ys.values()) {
                while (!nodes.isEmpty()) {
                    list.get(list.size() - 1).add(nodes.poll());
                }
            }
        }
        return list;
    }

    private void dfs(Node root, int x, int y, TreeMap<Integer, TreeMap<Integer, PriorityQueue<Integer>>> map) {
        if (root == null) {
            return;
        }
        if (!map.containsKey(x)) {
            map.put(x, new TreeMap<>());
        }

        TreeMap<Integer, PriorityQueue<Integer>> integerPriorityQueueTreeMap1 = map.get(x);
        boolean b = integerPriorityQueueTreeMap1.containsKey(y);

        if (!b) {
            TreeMap<Integer, PriorityQueue<Integer>> integerPriorityQueueTreeMap = map.get(x);
            integerPriorityQueueTreeMap.put(y, new PriorityQueue<>());
        }
        TreeMap<Integer, PriorityQueue<Integer>> integerPriorityQueueTreeMap = map.get(x);
        PriorityQueue<Integer> integers = integerPriorityQueueTreeMap.get(y);
        integers.offer(root.data);
        dfs(root.left, x - 1, y + 1, map);
        dfs(root.right, x + 1, y + 1, map);
    }


    public static void main(String[] args) {
        verticalOrderTraversal verticalOrderTraversal = new verticalOrderTraversal();

        // Inputting Data
        verticalOrderTraversal.root = new Node(1);
        verticalOrderTraversal.root.left = new Node(2);
        verticalOrderTraversal.root.right = new Node(3);
        verticalOrderTraversal.root.left.left = new Node(4);
        verticalOrderTraversal.root.left.right = new Node(5);

        // Recursive way
        List<List<Integer>> lists = verticalOrderTraversal.verticalTraversal(verticalOrderTraversal.root);

        System.out.println(lists);


    }


}
