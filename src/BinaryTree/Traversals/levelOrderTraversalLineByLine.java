package BinaryTree.Traversals;

import BinaryTree.Traversals.BinaryTree.Node;

public class levelOrderTraversalLineByLine {

    Node root;


    private void levelOrderRecursiveLineByLine(Node root)
    {
        int h = height(root);
        int i;
        for (i=1; i<=h; i++)
        {
            printGivenLevel(root, i);
            System.out.println();
        }
    }

    void printGivenLevel(Node root, int level)
    {
        if (root == null)
            return;
        if (level == 1)
            System.out.print(root.data + " ");
        else if (level > 1)
        {
            printGivenLevel(root.left, level-1);
            printGivenLevel(root.right, level-1);
        }
    }

    int height(Node root) {
        if (root == null) {
            return 0;
        } else {
            int leftHeight = height(root.left);
            int rightHeight = height(root.right);

            if (leftHeight > rightHeight) {
                return leftHeight + 1;
            } else {
                return rightHeight + 1;
            }

        }
    }



    public static void main(String[] args) {
        levelOrderTraversalLineByLine levelOrderTraversalLineByLine = new levelOrderTraversalLineByLine();

        // Inputting Data
        levelOrderTraversalLineByLine.root = new Node(1);
        levelOrderTraversalLineByLine.root.left = new Node(10);
        levelOrderTraversalLineByLine.root.right = new Node(30);
        levelOrderTraversalLineByLine.root.left.left = new Node(40);
        levelOrderTraversalLineByLine.root.right.right = new Node(50);

        // Recursive way
        levelOrderTraversalLineByLine.levelOrderRecursiveLineByLine(levelOrderTraversalLineByLine.root);



    }

}