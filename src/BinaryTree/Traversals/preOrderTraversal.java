package BinaryTree.Traversals;

import java.util.Stack;

import static BinaryTree.Traversals.BinaryTree.Node;

/*
If you know you need to explore the roots before inspecting any leaves,
        you pick pre-order because you will encounter all the roots before all of the leaves.*/

// Root (Print ) -> left -> right
public class preOrderTraversal {
    Node root;
    Stack<Node> stack = new Stack<>();


    private void preOrderRecursive(Node root) {
        // Base condition you must apply in every Binary Tree Questions
        if (root == null) {
            return;
        }
        System.out.print(root.data + "  ");

        preOrderRecursive(root.left);

        preOrderRecursive(root.right);
    }


    private void preOrderIterative(Node root) {
        // Base condition you must apply in every Binary Tree Questions
        if (root == null) {
            return;
        }
        stack.push(root); // Push the root than print in while loop

        /* Pop all items one by one. Do following for every popped item
         a) print it
         b) push its right child
         c) push its left child
         Note that right child is pushed first so that left is processed first */
        while (!stack.isEmpty()) {
            // Pop the top item from stack and print it
            Node popRoot = stack.pop();
            System.out.print(popRoot.data + " ");

            // Push right and left children of the popped node to stack
            // Right child is pushed before left child to make sure that left subtree is processed first.
            if (popRoot.right != null) {
                stack.push(popRoot.right);
            }
            if (popRoot.left != null) {
                stack.push(popRoot.left);
            }
        }
    }


    public static void main(String[] args) {
        preOrderTraversal preOrderTraversal = new preOrderTraversal();

        // Inputting Data
        preOrderTraversal.root = new Node(1);
        preOrderTraversal.root.left = new Node(3);
        preOrderTraversal.root.left.left = new Node(4);
        preOrderTraversal.root.right = new Node(2);
        preOrderTraversal.root.right.left = new Node(3);

        // Recursive way
        preOrderTraversal.preOrderRecursive(preOrderTraversal.root);

        System.out.println();

        // Iterative Pre Order Traversal
        preOrderTraversal.preOrderIterative(preOrderTraversal.root);

    }

}
