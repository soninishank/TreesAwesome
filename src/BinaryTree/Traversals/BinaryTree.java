package BinaryTree.Traversals;


// This is the basic binary tree structure used by all classes of binary tree
// This is the basic binary tree structure used by all classes of binary tree
public class BinaryTree {

    public static class Node //Inner static class
    {
        Node next; // Pointer

        public int data;
        public Node left;
        public Node right;
        public Node(int item)
        {
            data = item;
            left = null;
            right = null;
        }
    }

}

