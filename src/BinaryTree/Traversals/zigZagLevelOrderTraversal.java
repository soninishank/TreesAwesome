package BinaryTree.Traversals;

import BinaryTree.Traversals.BinaryTree.Node;

import java.util.Stack;

public class zigZagLevelOrderTraversal {

    Node root;
    Stack<Node> stack1 = new Stack<>();
    Stack<Node> stack2 = new Stack<>();


    public static void main(String[] args) {
        zigZagLevelOrderTraversal ZigZagLevelOrderTraversal = new zigZagLevelOrderTraversal();

        // Inputting Data
        ZigZagLevelOrderTraversal.root = new BinaryTree.Node(1);
        ZigZagLevelOrderTraversal.root.left = new BinaryTree.Node(10);
        ZigZagLevelOrderTraversal.root.right = new BinaryTree.Node(30);
        ZigZagLevelOrderTraversal.root.left.left = new BinaryTree.Node(40);
        ZigZagLevelOrderTraversal.root.right.right = new BinaryTree.Node(50);

        // Recursive way
        ZigZagLevelOrderTraversal.zigZagOrderIterative(ZigZagLevelOrderTraversal.root);


    }

    private void zigZagOrderIterative(Node root) {
        if (root == null) {
            return;
        }
        stack1.push(root);

        while (!stack1.isEmpty() || !stack2.isEmpty()) {
            while (!stack1.empty()) {
                Node temp = stack1.pop();
                System.out.print(temp.data + " ");

                // Note that is right is pushed before left
                if (temp.right != null) {
                    stack2.push(temp.right);
                }
                if (temp.left != null) {
                    stack2.push(temp.left);
                }
            }

            // Print nodes of current level from s2 and push nodes of
            // next level to s1
            while (!stack2.empty()) {
                Node temp = stack2.pop();
                System.out.print(temp.data + " ");

                // Note that is left is pushed before right
                if (temp.left != null)
                    stack1.push(temp.left);
                if (temp.right != null)
                    stack1.push(temp.right);
            }
        }

    }

}

