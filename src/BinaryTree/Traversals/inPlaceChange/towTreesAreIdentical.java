package BinaryTree.Traversals.inPlaceChange;

import BinaryTree.Traversals.BinaryTree;

import BinaryTree.Traversals.BinaryTree;
import BinaryTree.Traversals.BinaryTree.Node;

public class towTreesAreIdentical {

    Node root1, root2;


    public boolean identicalTrees(Node root1, Node root2) {
        // If both will be empty than it is true
        // Equal nullity denotes that this branch is the same (local equality)
        // This is a base case, but also handles being given two empty trees
        if (root1 == null && root2 == null) {
            return true;
        }
        if (root1 != null && root2 != null) {
            return root1.data == root2.data && identicalTrees(root1.left, root2.left) && identicalTrees(root1.right, root2.right);
        }
        return false;
    }


    public int isSameTree(Node root1, Node root2) {
        if (root1 == null && root2 == null) {
            return 1;
        }
        if (root1 != null && root2 != null) {
            boolean b = root1.data == root2.data;
            int sameTree = isSameTree(root1.left, root2.left);
            int sameTree1 = isSameTree(root1.right, root2.right);
            if (b == true && sameTree == 1 && sameTree1 == 1)
            {
                return 1;
            }
        }
        return 0;
    }


    public static void main(String[] args) {
        towTreesAreIdentical towTreesAreIdentical = new towTreesAreIdentical();

        // Inputting Data
        towTreesAreIdentical.root1 = new BinaryTree.Node(1);
        towTreesAreIdentical.root1.left = new BinaryTree.Node(2);
        towTreesAreIdentical.root1.right = new BinaryTree.Node(3);
        towTreesAreIdentical.root1.left.left = new BinaryTree.Node(4);
        towTreesAreIdentical.root1.left.right = new BinaryTree.Node(5);


        towTreesAreIdentical.root2 = new BinaryTree.Node(1);
        towTreesAreIdentical.root2.left = new BinaryTree.Node(2);
        towTreesAreIdentical.root2.right = new BinaryTree.Node(3);
        towTreesAreIdentical.root2.left.left = new BinaryTree.Node(4);
        towTreesAreIdentical.root2.left.right = new BinaryTree.Node(5);

        // Recursive way
        towTreesAreIdentical.isSameTree(towTreesAreIdentical.root1, towTreesAreIdentical.root2);

        System.out.println();


    }


}
