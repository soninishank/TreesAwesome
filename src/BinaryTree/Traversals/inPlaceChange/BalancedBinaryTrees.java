package BinaryTree.Traversals.inPlaceChange;

import BinaryTree.Traversals.BinaryTree;
import BinaryTree.Traversals.BinaryTree.Node;

public class BalancedBinaryTrees {

    Node root;

    // the difference between the heights of the two sub trees are not bigger than 1,
    // and both the left sub tree and right sub tree are also balanced.
    private boolean isBalanced(Node root) {
        if (root == null) {
            return true;
        }
        int depth = depth(root);
        if (depth == -1) {
            return false;
        } else {
            return true;
        }

    }

    private int depth(Node root) {
        if (root == null) {
            return 0;
        }
        int left = depth(root.left);
        int right = depth(root.right);
        int abs = Math.abs(left - right);
        if (left == -1 || right == -1 || abs > 1) {
            return -1;
        }
        int depth = Math.max(left, right) + 1;
        return depth;
    }


    public static void main(String[] args) {
        BalancedBinaryTrees BalancedBinaryTrees = new BalancedBinaryTrees();

        // Inputting Data
        BalancedBinaryTrees.root = new BinaryTree.Node(1);
        BalancedBinaryTrees.root.left = new BinaryTree.Node(2);
        BalancedBinaryTrees.root.right = new BinaryTree.Node(3);
        BalancedBinaryTrees.root.left.left = new BinaryTree.Node(4);
        BalancedBinaryTrees.root.left.right = new BinaryTree.Node(5);

        // Recursive way
        boolean symmetricTree = BalancedBinaryTrees.isBalanced(BalancedBinaryTrees.root);

        System.out.println(symmetricTree);


    }


}
