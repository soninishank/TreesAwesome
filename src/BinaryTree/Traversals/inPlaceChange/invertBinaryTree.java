package BinaryTree.Traversals.inPlaceChange;


// Convert a Binary Tree into its Mirror Tree

import BinaryTree.Traversals.levelOrderTraversal;

import static BinaryTree.Traversals.BinaryTree.*;

public class invertBinaryTree {
    Node root;

    private Node mirrorRecursive(Node root) {
        // time complexity is O(n)
        // Because of recursion, O(h) function calls will be placed on the stack in the worst case, where hh is the height of the tree. Because h\in O(n)h∈O(n), the space complexity is O(n).
      /* (1)Call Mirror for left - subtree i.e., Mirror(left - subtree)
         (2) Call Mirror for right - subtree i.e., Mirror(right - subtree)
         (3) Swap left and right subtrees.
                temp = left - subtree
             left - subtree = right - subtree
             right - subtree = temp  */
        // Base condition you must apply in every Binary Tree Questions
        if (root == null) {
            return root;
        }
        Node left = mirrorRecursive(root.left);
        Node right = mirrorRecursive(root.right);

        //Swap the left and right pointers
        root.left = right;
        root.right = left;

        return root;
    }

    private void mirrorIterative(Node root) {


    }


    public static void main(String[] args) {
        invertBinaryTree invertBinaryTree = new invertBinaryTree();


        // Inputting Data
        invertBinaryTree.root = new Node(1);
        invertBinaryTree.root.left = new Node(2);
        invertBinaryTree.root.right = new Node(3);
        invertBinaryTree.root.left.left = new Node(4);
        invertBinaryTree.root.left.right = new Node(5);

        // Recursive way
        Node node = invertBinaryTree.mirrorRecursive(invertBinaryTree.root);







    }


}
