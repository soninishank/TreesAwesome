package BinaryTree.Traversals.inPlaceChange;

import BinaryTree.Traversals.BinaryTree;
import BinaryTree.Traversals.BinaryTree.Node;

public class symmetricBinaryTree {
    Node root;


    // For two trees to be mirror images, the following three
    // conditions must be true
    // 1 - Their root node's key must be same
    // 2 - left subtree of left tree and right subtree
    //      of right tree have to be mirror images
    // 3 - right subtree of left tree and left subtree
    //      of right tree have to be mirror images
    private boolean isSymmetricTree(Node root1, Node root2) {
        if (root1 == null && root2 == null) {
            return true;
        }
        if (root1 != null && root2 != null && root1.data == root2.data) {
            return isSymmetricTree(root1.left, root2.right) && isSymmetricTree(root1.right, root2.left);
        }
        // If neither of the above conditions is true then root1 and root2 are mirror images
        return false;
    }


    private int isSymmetricTrees(Node root1, Node root2) {
        if (root1 == null && root2 == null) {
            return 1;
        }
        if (root1 != null && root2 != null && root1.data == root2.data) {
            int symmetricTrees = isSymmetricTrees(root1.left, root2.right);
            int symmetricTrees1 = isSymmetricTrees(root1.right, root2.left);
            if (symmetricTrees == 1 && symmetricTrees1 == 1) {
                return 1;
            }
        }
        // If neither of the above conditions is true then root1 and root2 are mirror images
        return 0;
    }


    public int isSymmetric(Node A) {
        int num = isSymmetricTrees(A, A);
        return num;
    }


    public static void main(String[] args) {
        symmetricBinaryTree symmetricBinaryTree = new symmetricBinaryTree();

        // Inputting Data
        symmetricBinaryTree.root = new BinaryTree.Node(1);
        symmetricBinaryTree.root.left = new BinaryTree.Node(2);
        symmetricBinaryTree.root.right = new BinaryTree.Node(3);
        symmetricBinaryTree.root.left.left = new BinaryTree.Node(4);
        symmetricBinaryTree.root.left.right = new BinaryTree.Node(5);

        // Recursive way
        boolean symmetricTree = symmetricBinaryTree.isSymmetricTree(symmetricBinaryTree.root, symmetricBinaryTree.root);

        System.out.println(symmetricTree);


    }


}

