package BinaryTree.LinkedList.BinaryTree.RootToLeaf;

import BinaryTree.Traversals.BinaryTree;
import BinaryTree.Traversals.BinaryTree.Node;


public class minDepthBinaryTree {
    Node root;

    public int minDepth(Node root) {
        if (root == null) {
            return 0;
        }
        int left = minDepth(root.left);
        int right = minDepth(root.right);
        if (left == 0 || right == 0) {
            int i = left + right + 1;
            return i;
        } else {
            int min = Math.min(left, right) + 1;
            return min;
        }
    }


    public static void main(String[] args) {
        minDepthBinaryTree minDepthBinaryTree = new minDepthBinaryTree();

        // Inputting Data
        minDepthBinaryTree.root = new Node(1);
        minDepthBinaryTree.root.left = new Node(2);
        minDepthBinaryTree.root.right = new Node(3);
        minDepthBinaryTree.root.left.left = new Node(4);
        minDepthBinaryTree.root.left.right = new Node(5);

        // Recursive way
        int test = minDepthBinaryTree.minDepth(minDepthBinaryTree.root);

        System.out.println(test);


    }
}
