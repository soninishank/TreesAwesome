package BinaryTree.LinkedList.BinaryTree.RootToLeaf;

import BinaryTree.Traversals.BinaryTree;
import BinaryTree.Traversals.BinaryTree.Node;

public class heightBinaryTree // Max Depth of Binary Tree
{

    Node root;


    private int heightRecursiveWay(Node root) {
        if (root == null) {
            return 0;
        }
        int leftDepth = heightRecursiveWay(root.left);
        int rightDepth = heightRecursiveWay(root.right);

        return 1 + Math.max(leftDepth, rightDepth);
    }


    public static void main(String[] args) {
        heightBinaryTree heightBinaryTree = new heightBinaryTree();

        // Inputting Data
        heightBinaryTree.root = new BinaryTree.Node(1);
        heightBinaryTree.root.left = new BinaryTree.Node(2);
        heightBinaryTree.root.right = new BinaryTree.Node(3);
        heightBinaryTree.root.left.left = new BinaryTree.Node(4);
        heightBinaryTree.root.left.right = new BinaryTree.Node(5);

        // Recursive way
        int i = heightBinaryTree.heightRecursiveWay(heightBinaryTree.root);

        System.out.println(i);


    }


}
