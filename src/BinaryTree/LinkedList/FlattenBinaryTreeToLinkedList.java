package BinaryTree.LinkedList;

import BinaryTree.Traversals.BinaryTree.Node;

public class FlattenBinaryTreeToLinkedList {

    Node root;


    //Great solution of the post order traversal in (right, left, root) order! Basically, the traversing order after flattening is pre order traversal in (root, left, right)
    //If we traverse the flattened tree in the reverse way, we would notice that [6->5->4->3->2->1] is in (right, left, root)
    // order of the original tree. So the reverse order after flattening is post order traversal in (right, left, root) order like [6->5->4->3->2->1]
    private Node prev;

    public Node flattenBinaryTreeRecursive(Node root) {
        if (root == null) {
            return root;
        }
        flattenBinaryTreeRecursive(root.right);
        flattenBinaryTreeRecursive(root.left);
        //  and then set each node's right pointer as the previous one in [6->5->4->3->2->1],
        //  as such the right pointer behaves similar to a link in the flattened tree(though technically,
        //  it's still a right child reference from the tree data structure's perspective) and set the left child as null before the end of one recursion by
        root.right = prev;
        root.left = null;
        prev = root;
        return prev;
    }


    public static void main(String[] args) {
        FlattenBinaryTreeToLinkedList flattenBinaryTreeToLinkedList = new FlattenBinaryTreeToLinkedList();


        // Inputting Data
        flattenBinaryTreeToLinkedList.root = new Node(1);
        flattenBinaryTreeToLinkedList.root.left = new Node(10);
        flattenBinaryTreeToLinkedList.root.right = new Node(30);
        flattenBinaryTreeToLinkedList.root.left.left = new Node(40);
        flattenBinaryTreeToLinkedList.root.right.right = new Node(50);

        // Recursive way
        Node node = flattenBinaryTreeToLinkedList.flattenBinaryTreeRecursive(flattenBinaryTreeToLinkedList.root);


    }
}
