package BinaryTree.LinkedList.BSTTraversal;


import BinaryTree.Traversals.BinaryTree.Node;

import java.util.HashSet;


public class TwoSumBinaryTree {
    public Node root;


    //This method also works for those who are not BSTs. The idea is to use a hashtable to save the values of the nodes in the BST.
    // Each time when we insert the value of a new node into the hashtable, we check if the hashtable contains k - node.val.
    public boolean findTarget(Node root, int target) {
        HashSet<Integer> hashSet = new HashSet<>();
        return dfs(root, hashSet, target);
    }

    public boolean dfs(Node root, HashSet<Integer> hashSet, int target) {
        int i = target - root.data;
        if (root == null) {
            return false;
        }
        if (hashSet.contains(i)) {
            return true;
        }
        hashSet.add(root.data);
        return dfs(root.left, hashSet, target) || dfs(root.right, hashSet, target);
    }

    public static void main(String[] args) {
        TwoSumBinaryTree twoSumBinaryTree = new TwoSumBinaryTree();

        // Inputting Data
        twoSumBinaryTree.root = new Node(1);
        twoSumBinaryTree.root.left = new Node(3);
        twoSumBinaryTree.root.left.left = new Node(4);
        twoSumBinaryTree.root.right = new Node(2);
        twoSumBinaryTree.root.right.left = new Node(3);

        // HashTable  way
        boolean target = twoSumBinaryTree.findTarget(twoSumBinaryTree.root, 7);

        System.out.println(target);



    }

}
