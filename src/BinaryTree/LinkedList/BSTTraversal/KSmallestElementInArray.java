package BinaryTree.LinkedList.BSTTraversal;

import BinaryTree.Traversals.BinaryTree.Node;

import java.util.ArrayList;

public class KSmallestElementInArray {

    Node root;

    public ArrayList<Integer> inorder(Node root, ArrayList<Integer> arr) 
    {
        if (root == null) {
            return arr;
        }
        inorder(root.left, arr);
        arr.add(root.data);
        inorder(root.right, arr);
        return arr;
    }

    //Recursive way 
    public int kthSmallestRecursive(Node root, int k) {
        ArrayList<Integer> nums = inorder(root, new ArrayList<Integer>());
        return nums.get(k - 1);
    }


    public static void main(String[] args) 
    {
        KSmallestElementInArray KSmallestElementInArray = new KSmallestElementInArray();

        // Inputting Data
        KSmallestElementInArray.root = new Node(1);
        KSmallestElementInArray.root.left = new Node(3);
        KSmallestElementInArray.root.left.left = new Node(4);
        KSmallestElementInArray.root.right = new Node(2);
        KSmallestElementInArray.root.right.left = new Node(3);

        // HashTable  way
        int target = KSmallestElementInArray.kthSmallestRecursive(KSmallestElementInArray.root, 3);

        System.out.println(target);


    }
}
